FROM ubuntu:mantic-20240530 AS build

RUN apt-get -y update \
    && apt-get -y install golang ca-certificates

WORKDIR /app

ADD main.go go.sum go.mod .

RUN go build -o hello_world main.go



FROM ubuntu:mantic-20240530 AS final

WORKDIR /app

COPY --from=build /app/hello_world /app/

ENTRYPOINT ["/app/app"]